 /* jshint esversion: 6 */
 
const webpack = require("webpack");

module.exports = {

    configureWebpack: {
        plugins: [
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
                Popper: ['popper.js', 'default']
              })
        ]
      },
      // externals: {
      //   google: 'google'
      // }
      devServer:{
        //start https
        // host: '0.0.0.0',
        // public: '138.250.190.244:8080',
        // port: '8080',
        // https:false
        https:true
      }
}