 /* jshint esversion: 6 */

import Vue from 'vue'
import App from './App.vue'
import router from './router'
//import store from './store'


import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "../other_element.css"

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

//internet connect with backend
import axios from 'axios';
import qs from 'qs';
Vue.prototype.$qs = qs;
Vue.prototype.$axios=axios;
//axios.defaults.baseURL ='https://localhost:8899';  
// axios.defaults.baseURL ='http://121.199.74.90:8899'; 
axios.defaults.baseURL ='https://www.cranfield-toilet.site:8899';   
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

//echart
import * as echarts from 'echarts'
Vue.prototype.$echarts = echarts

//google_maps
import '../node_modules/vue2-google-maps/dist/main'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyC58ayVz2HnsbIF76GVkTuNk6s9qZ-5qeM',
    language: 'english',
    loadCn: window.localStorage.getItem("lang") == "en",
  },
})


Vue.config.productionTip = false

new Vue({
  router,
  //store,
  render: h => h(App)
}).$mount('#app')
