 /* jshint esversion: 6 */

import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Start from '../components/Start.vue'
import Login from '../components/Login.vue'
import Register from '../components/Register.vue'
import add_Dev from '../views/add_Dev.vue'
import contr_Dev from '../views/contr_Dev.vue'
import ViewData from '../views/ViewData.vue'
import First from '../Detail/First.vue'
import Second from '../Detail/Second.vue'
import Third from '../Detail/Third.vue'
import Forth from '../Detail/Forth.vue'
import Fifth from '../Detail/Fifth.vue'
import Sixth from '../Detail/Sixth.vue'
import Seventh from '../Detail/Seventh.vue'
import Eighth from '../Detail/Eighth.vue'
import Ninth from '../Detail/Ninth.vue'
import Tenth from "../Detail/Tenth.vue"
import Eleventh from '../Detail/Eleventh.vue'
import Twelfth from '../Detail/Twelfth.vue'
import Thirteenth from '../Detail/Thirteenth.vue'
import Fourteenth from '../Detail/Fourteenth.vue'
import Fifteenth from '../Detail/Fifteenth.vue'
import Sixteenth from '../Detail/Sixteenth.vue'
import Seventeenth from '../Detail/Seventeenth.vue'
import Eighteenth from '../Detail/Eighteenth.vue'
import Nineteenth from '../Detail/Nineteenth.vue'
import Twenty from '../Detail/Twenty.vue'
import TwentyOne from '../Detail/TwentyOne.vue'
import TwentyTwo from "../Detail/TwentyTwo.vue"
import manage_Dev from '../views/manage_Dev.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Start',
    component: Start
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path:'/login',
    name:"Login",
    component:Login
  },
  {
    path:'/register',
    name:"Register",
    component:Register
  },
  {
    path:'/home',
    name:"Home",
    component:Home
  },
  {
    path:'/add_dev',
    name:'add_Dev',
    component:add_Dev
  },
  {
    path:'/contr_dev',
    name:'contr_Dev',
    component:contr_Dev,
    props:true
  },
  {
    path:'/manage',
    name:'manage_Dev',
    component:manage_Dev
  },
  {
    path:'/viewdata',
    name:'ViewData',
    component:ViewData,
    redirect:'/viewdata/first',
    children:[
      
      {
      path:'first',
      name:'First',
      component:First
    },{
      path:'second',
      name:'Second',
      component:Second
    },
    {
      path:'third',
      name:'Third',
      component:Third
    },
    {
      path:'forth',
      name:'Forth',
      component:Forth
    },
    {
      path:'fifth',
      name:'Fifth',
      component:Fifth
    },{
      path:'sixth',
      name:'Sixth',
      component:Sixth
    },
    {
      path:'seventh',
      name:'Seventh',
      component:Seventh
    },{
      path:'eighth',
      name:'Eighth',
      component:Eighth
    },{
      path:'ninth',
      name:'Ninth',
      component:Ninth
    },{
      path:'tenth',
      name:'Tenth',
      component:Tenth
    },{
      path:'eleventh',
      name:'Eleventh',
      component:Eleventh
    },{
      path:'twelfth',
      name:'Twelfth',
      component:Twelfth
    },
     {
      path:'thirteenth',
      name:'Thirteenth',
      component:Thirteenth
    },{
      path:'fourteenth',
      name:'Fourteenth',
      component:Fourteenth
    },
    {
      path:'fifteenth',
      name:'Fifteenth',
      component:Fifteenth
    },
    {
      path:'sixteenth',
      name:'Sixteenth',
      component:Sixteenth
    },
    {
      path:'seventeenth',
      name:'Seventeenth',
      component:Seventeenth
    },{
      path:'eighteenth',
      name:'Eighteenth',
      component:Eighteenth
    },
    {
      path:'nineteenth',
      name:'Nineteenth',
      component:Nineteenth
    },{
      path:'twenty',
      name:'Twenty',
      component:Twenty
    },{
      path:'twentyOne',
      name:'TwentyOne',
      component:TwentyOne
    },{
      path:'twentyTwo',
      name:'TwentyTwo',
      component:TwentyTwo
    }
  ]
  },
  {
    path:'/testDataShow',
    name:'testDataShow',
    component:()=>import('../components/ShowData.vue')
  },
  {
    path:'/datashowlist',
    name:'DataShowList',
    component:()=>import('../views/DataShowList.vue')
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})
// router.beforeEach((to, from, next) => {
//     if (to.path === '/') return next()
//     if (to.path === '/login') return next()
//     if (to.path === '/register') return next()

//     const tokenStr = window.sessionStorage.getItem('usertel')
//     if (!tokenStr) {
//         next('/')
//     }
//     next()
// })


export default router
